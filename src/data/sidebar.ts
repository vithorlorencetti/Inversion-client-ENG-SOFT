export const sidebar = [
  {
    id: '1',
    icon: 'material-symbols:home-outline',
    name: 'Dashboard',
    route: '/',
  },
  {
    id: '2',
    icon: 'fluent:document-data-24-regular',
    name: 'Carteiras',
    route: '/carteiras',
  },
  {
    id: '3',
    icon: 'iconoir:money-square',
    name: 'Operação',
    route: '/operacao',
  },
];
