export const tableHead = [
  {
    id: '1',
    title: 'Investimento',
  },
  {
    id: '2',
    title: 'Operação',
  },
  {
    id: '3',
    title: 'Quantidade',
  },
  {
    id: '4',
    title: 'Preço',
  },
  {
    id: '5',
    title: 'Rendimento',
  },
  {
    id: '6',
    title: 'Data',
  },
  {
    id: '7',
    title: 'Preço Médio',
  },
  {
    id: '8',
    title: 'Valor Total',
  },
];
